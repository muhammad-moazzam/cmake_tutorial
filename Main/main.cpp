#include <iostream>
#include "calculator.h"
#include "nlohmann/json.hpp"
using namespace std;
using json = nlohmann::json;
int main()
{
    calculator c;
    double b = c.add(5, 10);
    cout << "Hello World" << endl
         << b;
    json j = {
        {"pi", 3.141},
        {"happy", true},
        {"name", "Niels"},
        {"nothing", nullptr}};
    string s = j.dump(4);
    cout << s;
}